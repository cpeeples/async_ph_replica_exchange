import sys
sys.path.append("../")
import replica_exchange as re

phs = ['0.0', '0.5', '1.0', '1.5', '2.0', '2.5', '3.0', '3.5', '4.0', '4.5', '5.0', '5.5', '6.0', '6.5', '7.0', '7.5', '8.0', '8.5', '9.0', '9.5']
# First stage, as a start
re.do_replica_exchange(phs, 10, 'template.mdin', 'bbl.parm7', 'charmm_pme.parm', 'template.phmdin', 'output', concatenate_parm_file = 'bbl_solv_ion_mini.psf', initial_restart_file = 'equil4.rst7')
# Second stage, as a restart
re.do_replica_exchange(phs, 10, 'template.mdin', 'bbl.parm7', 'charmm_pme.parm', 'template.phmdin', 'output_2', concatenate_parm_file = 'bbl_solv_ion_mini.psf', restart_directory = 'output', restart = True)

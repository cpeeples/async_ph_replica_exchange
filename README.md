This Python code allows you run asynchoronous replica exchange simulations (details are given in the reference below).

If you publish work using the code, please cite the following article:

Jack A. Henderson, Neha Verma, Robert Harris, Ruibin Liu, and Jana Shen,
"Assessment of Proton-Coupled Conformational Dynamics of SARS and MERS Coronavirus Papain-like Proteases: Implication for Designing Broad-Spectrum Antiviral Inhibitors"
J. Chem. Phys. In press. 2020. 
doi: https://doi.org/10.1101/2020.06.30.181305 

To run the example replica exchange simulations:

1. Install required packages [`MDAnalysis`](https://www.mdanalysis.org/pages/installation_quick_start/) and [`f90nml`](https://pypi.org/project/f90nml/). (Note: it's recommended to create a new python/conda virtual environment to install these two packages to avoid possible conflicts.)
```bash
conda create -n py37 python=3.7
conda activate py37
conda install -c conda-forge mdanalysis
pip install f90nml
```

2. Set AMBERHOME (Amber installation directory) and CUDA_VISIBLE_DEVICES (GPU device ids) environmental variables.

3. Add the `async_pH_replica_exchange` directory to PYTHONPATH

3. Use `replica_exchange.py` as in the `examples/run_replica_exchange.py` example.

The python script `run_replica_exchange.py` contains an example to run the first stage and a following restart stage of an asynchoronous replica exchange CpHMD simulation for CHARMM force field, so it's necessary to include the 'concatenate_parm_file' keyword. For Amber force field, we can omit that keyword and keep the other parts. It's recommended to split a long run to several stages and let the GPU cards rest for a few mintues between two stages to avoid potential overheat. Also, according to our experience, the common command 'nvidia-smi' might freeze/crash the GPU cards during running our asynchoronous replica exchange simulations, so we'd better check whether a job is running by typing 'top' command and looking for a 'pmemd.cuda' process if necessary.

To improve speed, it's always benificial to reduce system IO by outputing files less frequently. To do that, the `template.in` file should be modified for real runs (i.e. not testing runs). We should set 'ntwe' to '0', 'ntwr' to a value not less than 'nstlim', 'ntwx' to multiples of 'nstlim' (i.e. ntwx = N * nstlim where N is an integer), and 'ntpr' to '0'. In such case, there will be no energy output file 'mden', no 'mdout' file, only one restart file per replica exchange attempt, and one trajectory file per N replica exchange attempts.
